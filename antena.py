import telnetlib
from tkinter import *
from datetime import date
import time

global ferramentas_perdidas
ferramentas_perdidas = 0

root = Tk()
root.resizable(0,0)
root.title("RFID")

frame_janela = Frame(None, width=1000, height=500, bg="white")
frame_janela.pack(side="left", expand=1, fill="both")

frame_logo = Frame(None, width=150, height=500, bg="black")
frame_logo.pack()
frame_logo.place(x=0, y=0)

label_logo = Label(frame_logo, text="VISÃO\nGERAL", fg="grey", bg="black")
label_logo["font"] = ("Calibri", "18")
label_logo.place(x=60, y=5)

frame_perdidas = Frame(None, width=150, height=80, bg="grey")
frame_perdidas.pack()
frame_perdidas.place(x=0, y=100)

label_perdidas = Label(frame_perdidas, text="PERDIDAS", fg="black", bg="grey")
label_perdidas["font"] = ("Calibri", "9")
label_perdidas.place(x=40, y=5)

frame_titulo = Frame(None, width=790, height=40, bg="black")
frame_titulo.pack()
frame_titulo.place(x=180, y=30)

label_titulo = Label(frame_titulo, text="FERRAMENTAS DETECTADAS", fg="white", bg="black")
label_titulo["font"] = ("Calibri", "18")
label_titulo.pack(ipadx=245)

frame_ferramenta = Frame(None, width=330, height=40, bg="black")
frame_ferramenta.pack()
frame_ferramenta.place(x=190, y=90)

label_ferramenta = Label(frame_ferramenta, text="FERRAMENTA", fg="white", bg="black")
label_ferramenta["font"] = ("Calibri", "14")
label_ferramenta.pack(ipadx=110, ipady=5)

frame_ferramenta_lista = Frame(None, width=330, height=350)
frame_ferramenta_lista.pack()
frame_ferramenta_lista.place(x=190, y=140)

frame_portão = Frame(None, width=130, height=40, bg="black")
frame_portão.pack()
frame_portão.place(x=530, y=90)

label_portão = Label(frame_portão, text="PORTÃO", fg="white", bg="black")
label_portão["font"] = ("Calibri", "14")
label_portão.pack(ipadx=30, ipady=5)

frame_portão_lista = Frame(None, width=130, height=350)
frame_portão_lista.pack()
frame_portão_lista.place(x=530, y=140)

frame_horário = Frame(None, width=130, height=40, bg="black")
frame_horário.pack()
frame_horário.place(x=670, y=90)

label_horário = Label(frame_horário, text="HORÁRIO", fg="white", bg="black")
label_horário["font"] = ("Calibri", "14")
label_horário.pack(ipadx=25, ipady=5)

frame_horário_lista = Frame(None, width=130, height=350)
frame_horário_lista.pack()
frame_horário_lista.place(x=670, y=140)

frame_status = Frame(None, width=130, height=40, bg="black")
frame_status.pack()
frame_status.place(x=810, y=90)

label_status = Label(frame_status, text="STATUS", fg="white", bg="black")
label_status["font"] = ("Calibri", "14")
label_status.pack(ipadx=33, ipady=5)

frame_status_lista = Frame(None, width=130, height=350)
frame_status_lista.pack()
frame_status_lista.place(x=810, y=140)

sair = Button(frame_logo, text=" SAIR ", command=root.destroy, fg="black", bg="grey")
sair["font"] = ("Calibri", "10")
sair.place(x=20, y=455)

class Application:
    
    def __init__(self, master=None):
        
        self.label_pendentes = Label(frame_perdidas, text=ferramentas_perdidas, fg="red", bg="grey")
        self.label_pendentes["font"] = ("Calibri", "30")
        self.label_pendentes.place(x=60, y=20)

Application(root)
root.update_idletasks()
root.update()

tn = telnetlib.Telnet('192.168.15.20', 14150)

apertadeira1 = "000000000000000000000026"
ultimo_t_apertadeira1 = 0
manutencao1 = 0
portao_detectada1 = 0

apertadeira2 = "001234567890123456789012"
ultimo_t_apertadeira2 = 0
manutencao2 = 0
portao_detectada2 = 0

apertadeira3 = "020105120000000000001E2C"
ultimo_t_apertadeira3 = 0
manutencao3 = 0
portao_detectada3 = 0

apertadeira4 = "500833B2DDD9014000000000"
ultimo_t_apertadeira4 = 0
manutencao4 = 0
portao_detectada4 = 0

apertadeira5 = "300833B2DDD9014000000000"
ultimo_t_apertadeira5 = 0
manutencao5 = 1
portao_detectada5 = 0

apertadeira6 = "3005FB63AC1F3681EC880468"
ultimo_t_apertadeira6 = 0
manutencao6 = 1
portao_detectada6 = 0

perdidas = []
portao_perdidas = []
horario_perdidas = []
status_perdidas = []    

fer_perdida = []
portao_perdido = []
hora_perdida = []
status_perdida = []       

while(1):
    antena = tn.read_until(str.encode(","))
    tag = tn.read_until(str.encode(","))
    tempo = tn.read_until(str.encode("\r"))
    tn.read_until(str.encode("\n"))

    str_antena = antena.decode()
    str_tag = tag.decode()
    str_tempo = tempo.decode()

    str_antena = str_antena.replace(",", "")
    str_tag = str_tag.replace(",", "")
        
    if(str_tag == apertadeira1):
        
        if(int(tempo) - ultimo_t_apertadeira1 > 1999999 and portao_detectada1 == 0):
            t = time.localtime()
            current_day = date.today().strftime('%d/%m/%Y')
            current_time = time.strftime("%H:%M:%S", t)
            portao_detectada1 = int(str_antena)
            perdidas.append(apertadeira1) 
            portao_perdidas.append(str_antena)
            horario_perdidas.append(current_day + "\n" + current_time)
            if(manutencao1 == 1):
                status_perdidas.append("Manutenção")
            else:
                status_perdidas.append("Perdida")
                ferramentas_perdidas += 1
        elif(int(tempo) - ultimo_t_apertadeira1 > 1999999 and portao_detectada1 != 0):
            portao_detectada1 = 0
            portao_perdidas.pop(perdidas.index(apertadeira1))
            horario_perdidas.pop(perdidas.index(apertadeira1))
            if(status_perdidas[perdidas.index(apertadeira1)] == "Perdida"):
                ferramentas_perdidas -= 1
            status_perdidas.pop(perdidas.index(apertadeira1))
            perdidas.remove(apertadeira1) 
            
            
        ultimo_t_apertadeira1 = int(tempo)

    elif(str_tag == apertadeira2):

        if(int(tempo) - ultimo_t_apertadeira2 > 1999999 and portao_detectada2 == 0):
            t = time.localtime()
            current_day = date.today().strftime('%d/%m/%Y')
            current_time = time.strftime("%H:%M:%S", t)
            portao_detectada2 = int(str_antena)
            perdidas.append(apertadeira2)
            portao_perdidas.append(str_antena)
            horario_perdidas.append(current_day + "\n" + current_time)
            if(manutencao2 == 1):
                status_perdidas.append("Manutenção")
            else:
                status_perdidas.append("Perdida")
                ferramentas_perdidas += 1
        elif(int(tempo) - ultimo_t_apertadeira2 > 1999999 and portao_detectada2 != 0):
            portao_detectada2 = 0
            portao_perdidas.pop(perdidas.index(apertadeira2))
            horario_perdidas.pop(perdidas.index(apertadeira2))
            if(status_perdidas[perdidas.index(apertadeira2)] == "Perdida"):
                ferramentas_perdidas -= 1
            status_perdidas.pop(perdidas.index(apertadeira2))
            perdidas.remove(apertadeira2)
            
        ultimo_t_apertadeira2 = int(tempo)

    elif(str_tag == apertadeira3):

        if(int(tempo) - ultimo_t_apertadeira3 > 1999999 and portao_detectada3 == 0):
            t = time.localtime()
            current_day = date.today().strftime('%d/%m/%Y')
            current_time = time.strftime("%H:%M:%S", t)
            portao_detectada3 = int(str_antena)
            perdidas.append(apertadeira3)
            portao_perdidas.append(str_antena)
            horario_perdidas.append(current_day + "\n" + current_time)
            if(manutencao3 == 1):
                status_perdidas.append("Manutenção")
            else:
                status_perdidas.append("Perdida")
                ferramentas_perdidas += 1
        elif(int(tempo) - ultimo_t_apertadeira3 > 1999999 and portao_detectada3 != 0):
            portao_detectada3 = 0
            portao_perdidas.pop(perdidas.index(apertadeira3))
            horario_perdidas.pop(perdidas.index(apertadeira3))
            if(status_perdidas[perdidas.index(apertadeira3)] == "Perdida"):
                ferramentas_perdidas -= 1
            status_perdidas.pop(perdidas.index(apertadeira3))
            perdidas.remove(apertadeira3)
            
        ultimo_t_apertadeira3 = int(tempo)

    elif(str_tag == apertadeira4):

        if(int(tempo) - ultimo_t_apertadeira4 > 1999999 and portao_detectada4 == 0):
            t = time.localtime()
            current_day = date.today().strftime('%d/%m/%Y')
            current_time = time.strftime("%H:%M:%S", t)
            portao_detectada4 = int(str_antena)
            perdidas.append(apertadeira4)
            portao_perdidas.append(str_antena)
            horario_perdidas.append(current_day + "\n" + current_time)
            if(manutencao4 == 1):
                status_perdidas.append("Manutenção")
            else:
                status_perdidas.append("Perdida")
                ferramentas_perdidas += 1
        elif(int(tempo) - ultimo_t_apertadeira4 > 1999999 and portao_detectada4 != 0):
            portao_detectada4 = 0
            portao_perdidas.pop(perdidas.index(apertadeira4))
            horario_perdidas.pop(perdidas.index(apertadeira4))
            if(status_perdidas[perdidas.index(apertadeira4)] == "Perdida"):
                ferramentas_perdidas -= 1
            status_perdidas.pop(perdidas.index(apertadeira4))
            perdidas.remove(apertadeira4)
            
        ultimo_t_apertadeira4 = int(tempo)

    elif(str_tag == apertadeira5):

        if(int(tempo) - ultimo_t_apertadeira5 > 1999999 and portao_detectada5 == 0):
            t = time.localtime()
            current_day = date.today().strftime('%d/%m/%Y')
            current_time = time.strftime("%H:%M:%S", t)
            portao_detectada5 = int(str_antena)
            perdidas.append(apertadeira5)
            portao_perdidas.append(str_antena)
            horario_perdidas.append(current_day + "\n" + current_time)
            if(manutencao5 == 1):
                status_perdidas.append("Manutenção")
            else:
                status_perdidas.append("Perdida")
                ferramentas_perdidas += 1
        elif(int(tempo) - ultimo_t_apertadeira5 > 1999999 and portao_detectada5 != 0):
            portao_detectada5 = 0
            portao_perdidas.pop(perdidas.index(apertadeira5))
            horario_perdidas.pop(perdidas.index(apertadeira5))
            if(status_perdidas[perdidas.index(apertadeira5)] == "Perdida"):
                ferramentas_perdidas -= 1
            status_perdidas.pop(perdidas.index(apertadeira5))
            perdidas.remove(apertadeira5)
            
        ultimo_t_apertadeira5 = int(tempo)

    elif(str_tag == apertadeira6):

        if(int(tempo) - ultimo_t_apertadeira6 > 1999999 and portao_detectada6 == 0):
            t = time.localtime()
            current_day = date.today().strftime('%d/%m/%Y')
            current_time = time.strftime("%H:%M:%S", t)
            portao_detectada6 = int(str_antena)
            perdidas.append(apertadeira6)
            portao_perdidas.append(str_antena)
            horario_perdidas.append(current_day + "\n" + current_time)
            if(manutencao6 == 1):
                status_perdidas.append("Manutenção")
            else:
                status_perdidas.append("Perdida")
                ferramentas_perdidas += 1
        elif(int(tempo) - ultimo_t_apertadeira6 > 1999999 and portao_detectada6 != 0):
            portao_detectada6 = 0
            portao_perdidas.pop(perdidas.index(apertadeira6))
            horario_perdidas.pop(perdidas.index(apertadeira6))
            if(status_perdidas[perdidas.index(apertadeira6)] == "Perdida"):
                ferramentas_perdidas -= 1
            status_perdidas.pop(perdidas.index(apertadeira6))
            perdidas.remove(apertadeira6)
            
        ultimo_t_apertadeira6 = int(tempo)
    
    index = 0
    cor = ""
    for x in perdidas:
        if(status_perdidas[perdidas.index(x)] == "Perdida"): cor = "red"
        else: cor = "green"
        fer_perdida.append(Label(frame_ferramenta_lista, text=("  " + x + "  "), fg="white", bg=cor))
    for y in range(len(fer_perdida)):
        fer_perdida[y]["font"] = ("Calibri", "14")
        fer_perdida[y].place(x=10, y=10 + index)
        index += 40
    
    index = 0
    for x in portao_perdidas:
        #if(status_perdidas[portao_perdidas.index(x)] == "Perdida"): cor = "red"
        #else: cor = "green"
        portao_perdido.append(Label(frame_portão_lista, text=("  " + x + "  "), fg="white", bg="black"))
    for y in range(len(portao_perdido)):
        portao_perdido[y]["font"] = ("Calibri", "14")
        portao_perdido[y].place(x=10, y=10 + index)
        index += 40
    
    index = 0
    for x in horario_perdidas:
        if(status_perdidas[horario_perdidas.index(x)] == "Perdida"): cor = "red"
        else: cor = "green"
        hora_perdida.append(Label(frame_horário_lista, text=("  " + x + "  "), fg="white", bg=cor))
    for y in range(len(hora_perdida)):
        hora_perdida[y]["font"] = ("Calibri", "8")
        hora_perdida[y].place(x=10, y=10 + index)
        index += 40

    index = 0
    for x in status_perdidas:
        if(status_perdidas[status_perdidas.index(x)] == "Perdida"): cor = "red"
        else: cor = "green"
        status_perdida.append(Label(frame_status_lista, text=(" " + x + " "), fg="white", bg=cor))
    for y in range(len(status_perdida)):
        status_perdida[y]["font"] = ("Calibri", "14")
        status_perdida[y].place(x=10, y=10 + index)
        index += 40

    Application(root)
    root.update_idletasks()
    root.update()

    for z in range(len(fer_perdida)):
        fer_perdida[0].destroy()
        fer_perdida.pop(0)

    for z in range(len(portao_perdido)):
        portao_perdido[0].destroy()
        portao_perdido.pop(0)

    for z in range(len(hora_perdida)):
        hora_perdida[0].destroy()
        hora_perdida.pop(0)

    for z in range(len(status_perdida)):
        status_perdida[0].destroy()
        status_perdida.pop(0)
    
    time.sleep(1)
